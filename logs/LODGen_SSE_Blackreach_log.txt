============================================================
Skyrim/Fallout Object/Terrain LOD Generator 3.0.0.0
Created by Ehamloptiran and Zilav
Updated by Sheson

Log started at 3:49:20 PM
Thread split: 2
Game Mode: SSE
Worldspace: Blackreach 
Fix Tangents: False, False, False, False
Generate Tangents: True, True, True, True
Generate Vertex Colors: True, True, True, True
Merge Vertex Colors: True, True, True, True
Merge Meshes: True
Grouping: False
Remove Faces under Terrain: True
Remove Faces under Water: True
Remove Faces Z-Shift: 10
Use HD Flag: True
Ignore Materials: False
Alpha DoubleSided: False
Default Alpha Threshold: 128
Use Source Alpha Threshold: False
Use Backlight Power: False
Use Decal Flag: False
Specific level: No
Specific quad: No
Max Level: 32
Output: C:\Modding\DynDOLOD\DynDOLOD_Output\Meshes\Terrain\Blackreach\Objects\
Using UV Atlas: C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_ObjectAtlasMap_Blackreach.txt
Using Flat Textures: C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_FlatTextures.txt
Using Alt Textures: C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_AltTextures_Blackreach.txt
Generating object LOD meshes for worldspace Blackreach
Reading C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_Terrain_Blackreach.bin
Finished LOD level 4 coord [5, -1] [3750/2960]
Finished LOD level 4 coord [-3, 3] [27339/21193]
Shader Type 11 = FXWaterfallBodyTallInner01 = FXWaterfallBodyTall_static, Cell (1, -1): 0 statics
Finished LOD level 4 coord [1, -1] [67719/52045]
Finished LOD level 4 coord [5, 3] [7045/5974]
Shader Type 11 = FXWaterfallBodyTallInner01 = FXWaterfallBodyTall_static, Cell (1, -1): 0 statics
Shader Type 11 = FXWaterfallBodyTallInner01 = FXWaterfallBodyTall_static, Cell (1, 3): 0 statics
Finished LOD level 8 coord [1, -1] [77910/59648]
Finished LOD level 4 coord [1, 3] [51374/39751]
Shader Type 11 = FXWaterfallBodyTallInner01 = FXWaterfallBodyTall_static, Cell (1, -9): 0 statics
Finished LOD level 8 coord [1, -9] [7752/6459]
Shader Type 11 = FXWaterfallBodyTallInner01 = FXWaterfallBodyTall_static, Cell (1, -5): 0 statics
Finished LOD level 4 coord [1, -5] [12037/10539]
Finished LOD level 8 coord [-7, -1] [35404/27206]
Shader Type 11 = FXWaterfallBodySlopeMesh01 = FXWaterfallBodySlope_static, Cell (-7, -9): 0 statics
Finished LOD level 8 coord [-7, -9] [8023/5010]
Finished LOD level 4 coord [-3, -1] [37940/30731]
Shader Type 11 = FXWaterfallBodySlopeMesh01 = FXWaterfallBodySlope_static, Cell (-3, -5): 0 statics
Finished LOD level 4 coord [-3, -5] [11786/7898]
Shader Type 11 = FXWaterfallBodySlopeMesh01 = FXWaterfallBodySlope_static, Cell (-7, -9): 0 statics
Finished LOD level 16 coord [-7, -9] [69583/56174]
LOD level 4 total triangles 218990 reduced to 171091
LOD level 8 total triangles 129089 reduced to 98323
LOD level 16 total triangles 69583 reduced to 56174
Log ended at 3:49:29 PM (0:9)
Code: 0
