============================================================
Skyrim/Fallout Object/Terrain LOD Generator 3.0.0.0
Created by Ehamloptiran and Zilav
Updated by Sheson

Log started at 3:47:22 PM
Thread split: 2
Game Mode: SSE
Worldspace: MarkarthWorld 
Fix Tangents: False, False, False, False
Generate Tangents: True, True, True, True
Generate Vertex Colors: True, True, True, True
Merge Vertex Colors: True, True, True, True
Merge Meshes: True
Grouping: False
Remove Faces under Terrain: True
Remove Faces under Water: True
Remove Faces Z-Shift: 10
Use HD Flag: True
Ignore Materials: False
Alpha DoubleSided: False
Default Alpha Threshold: 128
Use Source Alpha Threshold: False
Use Backlight Power: False
Use Decal Flag: False
Specific level: No
Specific quad: No
Max Level: 32
Output: C:\Modding\DynDOLOD\DynDOLOD_Output\Meshes\Terrain\MarkarthWorld\Objects\
Using UV Atlas: C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_ObjectAtlasMap_MarkarthWorld.txt
Using Flat Textures: C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_FlatTextures.txt
Using Alt Textures: C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_AltTextures_MarkarthWorld.txt
Generating object LOD meshes for worldspace MarkarthWorld
Reading C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_Terrain_MarkarthWorld.bin
Finished LOD level 4 coord [-40, 1] [10651/5406]
Finished LOD level 4 coord [-48, 1] [4188/3357]
Shader Type 11 = FXWaterfallBodyTallInner01 = FXWaterfallBodyTall_static, Cell (-48, -3): 0 statics
Shader Type 11 = FXWaterfallBodyTallInner01 = FXWaterfallBodyTall_static_T, Cell (-48, -3): 0 statics
Finished LOD level 4 coord [-44, 1] [33108/25614]
Finished LOD level 8 coord [-48, -3] [45227/31489]
Finished LOD level 8 coord [-40, -3] [9075/4151]
Shader Type 11 = FXWaterfallBodyTallInner01 = FXWaterfallBodyTall_static_T, Cell (-44, -3): 0 statics
Shader Type 11 = FXWaterfallBodyTallInner01 = FXWaterfallBodyTall_static, Cell (-44, -3): 0 statics
Shader Type 11 = FXWaterfallBodyTallInner01 = FXWaterfallBodyTall_static, Cell (-44, -3): 0 statics
Finished LOD level 4 coord [-44, -3] [32283/22840]
Finished LOD level 4 coord [-48, -3] [2891/1920]
Finished LOD level 4 coord [-40, -3] [3785/1691]
Shader Type 11 = FXWaterfallBodyTallInner01 = FXWaterfallBodyTall_static, Cell (-48, -3): 0 statics
Finished LOD level 16 coord [-48, -3] [30100/18378]
LOD level 4 total triangles 86906 reduced to 60828
LOD level 8 total triangles 54302 reduced to 35640
LOD level 16 total triangles 30100 reduced to 18378
Log ended at 3:47:31 PM (0:8)
Code: 0
