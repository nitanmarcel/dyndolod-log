============================================================
Skyrim/Fallout Object/Terrain LOD Generator 3.0.0.0
Created by Ehamloptiran and Zilav
Updated by Sheson

Log started at 3:47:30 PM
Game Mode: UNDERTERRAINSSE
Worldspace: MarkarthWorld 
Generating terrain LOD meshes for worldspace MarkarthWorld
Reading C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_Terrain_MarkarthWorld.bin
Data from -48,-3 to -31,5
Protect Cell Border LOD 4: False
Specific level: 32
Max Level: 32
Specific quad: No
Output: C:\Modding\DynDOLOD\DynDOLOD_Output\Meshes\Terrain\MarkarthWorld\
Quality LOD32: 10   Max Vertices: 32767   Optimize Unseen: -1
Finished C:\Modding\DynDOLOD\DynDOLOD_Output\Meshes\Terrain\MarkarthWorld\MarkarthWorld_Underside.nif
Log ended at 3:47:31 PM (0:1)
Code: 0
