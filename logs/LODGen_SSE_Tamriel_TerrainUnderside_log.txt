============================================================
Skyrim/Fallout Object/Terrain LOD Generator 3.0.0.0
Created by Ehamloptiran and Zilav
Updated by Sheson

Log started at 3:45:29 PM
Game Mode: UNDERTERRAINSSE
Worldspace: Tamriel 
Generating terrain LOD meshes for worldspace Tamriel
Reading C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_Terrain_Tamriel.bin
Data from -57,-43 to 61,50
Protect Cell Border LOD 4: False
Specific level: 32
Max Level: 32
Specific quad: No
Output: C:\Modding\DynDOLOD\DynDOLOD_Output\Meshes\Terrain\Tamriel\
Quality LOD32: 10   Max Vertices: 32767   Optimize Unseen: -1
Finished C:\Modding\DynDOLOD\DynDOLOD_Output\Meshes\Terrain\Tamriel\Tamriel_Underside.nif
Log ended at 3:45:39 PM (0:9)
Code: 0
