============================================================
Skyrim/Fallout Object/Terrain LOD Generator 3.0.0.0
Created by Ehamloptiran and Zilav
Updated by Sheson

Log started at 3:48:26 PM
Game Mode: UNDERTERRAINSSE
Worldspace: DeepwoodRedoubtWorld 
Generating terrain LOD meshes for worldspace DeepwoodRedoubtWorld
Reading C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_Terrain_DeepwoodRedoubtWorld.bin
Data from -39,15 to -27,24
Protect Cell Border LOD 4: False
Specific level: 32
Max Level: 32
Specific quad: No
Output: C:\Modding\DynDOLOD\DynDOLOD_Output\Meshes\Terrain\DeepwoodRedoubtWorld\
Quality LOD32: 10   Max Vertices: 32767   Optimize Unseen: -1
Finished C:\Modding\DynDOLOD\DynDOLOD_Output\Meshes\Terrain\DeepwoodRedoubtWorld\DeepwoodRedoubtWorld_Underside.nif
Log ended at 3:48:27 PM (0:1)
Code: 0
