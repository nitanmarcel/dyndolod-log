============================================================
Skyrim/Fallout Object/Terrain LOD Generator 3.0.0.0
Created by Ehamloptiran and Zilav
Updated by Sheson

Log started at 3:49:01 PM
Thread split: 2
Game Mode: SSE
Worldspace: DLC01FalmerValley 
Fix Tangents: False, False, False, False
Generate Tangents: True, True, True, True
Generate Vertex Colors: True, True, True, True
Merge Vertex Colors: True, True, True, True
Merge Meshes: True
Grouping: False
Remove Faces under Terrain: True
Remove Faces under Water: True
Remove Faces Z-Shift: 10
Use HD Flag: True
Ignore Materials: False
Alpha DoubleSided: False
Default Alpha Threshold: 128
Use Source Alpha Threshold: False
Use Backlight Power: False
Use Decal Flag: False
Specific level: No
Specific quad: No
Max Level: 32
Output: C:\Modding\DynDOLOD\DynDOLOD_Output\Meshes\Terrain\DLC01FalmerValley\Objects\
Using UV Atlas: C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_ObjectAtlasMap_DLC01FalmerValley.txt
Using Flat Textures: C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_FlatTextures.txt
Using Alt Textures: C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_AltTextures_DLC01FalmerValley.txt
Generating object LOD meshes for worldspace DLC01FalmerValley
Reading C:\Modding\DynDOLOD\Edit Scripts\Export\LODGen_SSE_Terrain_DLC01FalmerValley.bin
Finished LOD level 4 coord [-12, -9] [3662/1760]
Finished LOD level 4 coord [4, -5] [5687/2207]
Finished LOD level 4 coord [0, 7] [2901/1531]
Finished LOD level 4 coord [-12, 7] [844/844]
Finished LOD level 4 coord [-4, -1] [35727/21430]
Finished LOD level 4 coord [0, 11] [1678/1567]
Finished LOD level 4 coord [4, -9] [1520/1113]
Finished LOD level 4 coord [0, -13] [844/618]
Finished LOD level 4 coord [-8, 3] [42482/25256]
Finished LOD level 8 coord [-8, -5] [101538/52717]
Shader Type 11 = FXWaterfallBodyTallInner02 = FXWaterfallBodyTall02_static, Cell (0, -5): 0 statics
Finished LOD level 4 coord [0, -5] [41956/23182]
Shader Type 11 = FXWaterfallBodyTallInner01 = FXWaterfallBodyTallnoLOD, Cell (-8, 3): 0 statics
Finished LOD level 8 coord [-8, 3] [66426/37813]
Finished LOD level 4 coord [-4, -5] [57869/28727]
Shader Type 11 = FXWaterfallBodyTallInner02 = FXWaterfallBodyTall02_static, Cell (0, -5): 0 statics
Finished LOD level 4 coord [0, 3] [33837/20375]
Finished LOD level 8 coord [0, -5] [64408/33297]
Finished LOD level 8 coord [0, 3] [33464/17178]
Shader Type 11 = FXWaterfallBodyTallInner01 = FXWaterfallBodyTallnoLOD, Cell (-4, 3): 0 statics
Finished LOD level 8 coord [-16, -5] [17825/8431]
Finished LOD level 4 coord [-4, 3] [53574/30016]
Finished LOD level 8 coord [-8, -13] [21769/11989]
Finished LOD level 4 coord [-12, -5] [19959/10200]
Finished LOD level 8 coord [-16, 3] [2942/1718]
Finished LOD level 8 coord [0, -13] [6695/3396]
Finished LOD level 8 coord [-8, 11] [1121/1121]
Finished LOD level 8 coord [8, 3] [2372/1882]
Finished LOD level 8 coord [8, -5] [1135/532]
Finished LOD level 8 coord [-16, -13] [2507/1196]
Finished LOD level 8 coord [0, 11] [1155/1023]
Finished LOD level 4 coord [-8, -5] [48935/32900]
Finished LOD level 4 coord [0, -1] [46952/28226]
Finished LOD level 16 coord [-16, -13] [69021/38663]
Finished LOD level 4 coord [-8, -9] [24054/15990]
Finished LOD level 4 coord [-12, -1] [8470/3534]
Shader Type 11 = FXWaterfallBodyTallInner01 = FXWaterfallBodyTallnoLOD, Cell (-16, 3): 0 statics
Finished LOD level 16 coord [-16, 3] [42591/24158]
Finished LOD level 4 coord [4, -1] [26225/15746]
Finished LOD level 4 coord [-12, 3] [3409/1684]
Shader Type 11 = FXWaterfallBodyTallInner02 = FXWaterfallBodyTall02_static, Cell (0, -13): 0 statics
Finished LOD level 16 coord [0, -13] [36986/19159]
Finished LOD level 16 coord [0, 3] [20480/11106]
Finished LOD level 4 coord [-8, -1] [39405/19756]
Finished LOD level 4 coord [0, -9] [8212/3515]
Finished LOD level 4 coord [-4, 7] [16779/13247]
Finished LOD level 4 coord [-4, 11] [2057/2057]
Finished LOD level 4 coord [4, 7] [3301/1539]
Finished LOD level 4 coord [-4, -9] [16025/8247]
Finished LOD level 4 coord [8, 3] [3436/2703]
Finished LOD level 4 coord [8, -1] [1640/796]
Finished LOD level 4 coord [-8, 7] [4548/1390]
Finished LOD level 4 coord [4, 3] [18221/9063]
LOD level 4 total triangles 574209 reduced to 329219
LOD level 8 total triangles 323357 reduced to 172293
LOD level 16 total triangles 169078 reduced to 93086
,textures\landscape\riverbededge.dds: 830 -> 1042 | 98, 103
Log ended at 3:49:17 PM (0:16)
Code: 0
